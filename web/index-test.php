<?php

// NOTE: Make sure this file is not accessible when deployed to production
if (!in_array(@$_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])) {
    die('You are not allowed to access this file.');
}

require __DIR__ . '/../bootstrap.php';
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../environment.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

if (!isInstalled()) {
    die('Error! Create and configure .env file');
}

$config = require __DIR__ . '/../config/test.php';

(new yii\web\Application($config))->run();
