$(function() {
    var COUNT_ACTION = {
        INCREMENT: 1,
        DECREMENT: 2
    };

    $('.button-like').click(function() {
        var $btn = $(this);
        
        toggleLike($btn);

        $.ajax({
            url: '/ajax/toggle-like',
            type:'post',
            data: {
                _csrf: yii.getCsrfToken(),
                entity: $btn.attr('data-entity'),
                target_id: $btn.attr('data-entity-id')
            },
            dataType: 'json',
            success: (data) => {
                if(data.status != 'success') {
                    toggleLike($btn);
                    alert(data.message);
                }
            }
        })
    });

    var toggleLike = function($likeBtn) {
        var $likeCount = $likeBtn.find('.like-count');

        if($likeBtn.hasClass('like-active')) {
            $likeBtn.removeClass('like-active');
            changeLikeCount($likeCount, COUNT_ACTION.DECREMENT);
        } else {
            $likeBtn.addClass('like-active');
            changeLikeCount($likeCount, COUNT_ACTION.INCREMENT);
        }
    }

    var changeLikeCount = function($el, action) {
        var likeCount = parseInt($el.html());
        if(action === COUNT_ACTION.INCREMENT) {
            likeCount++;
        } else if(action === COUNT_ACTION.DECREMENT) {
            likeCount--;
        }

        if(likeCount < 0) {
            likeCount = 0;
        }

        $el.html(likeCount);
    }
});