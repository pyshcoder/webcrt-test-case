<?php
require __DIR__ . '/../bootstrap.php';
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../environment.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

if (!isInstalled()) {
    die('Error! Create and configure .env file');
}

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
