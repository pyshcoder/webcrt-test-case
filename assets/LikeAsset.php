<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * Like widget asset bundle
 */
class LikeAsset extends AssetBundle
{
    public $css = [
        'css/like.css',
    ];
    public $js = [
        'js/like.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
