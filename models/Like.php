<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "likes".
 *
 * @property int $id
 * @property string $entity
 * @property int $target_id
 * @property int $user_id
 *
 * @property User $user
 */
class Like extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'likes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity', 'target_id', 'user_id'], 'required'],
            [['target_id', 'user_id'], 'integer'],
            [['entity'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'entity' => Yii::t('app', 'Entity'),
            'target_id' => Yii::t('app', 'Target ID'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Counts likes for given entity
     *
     * @param \yii\db\ActiveRecord $entity
     * @return int Like count
     */
    public static function countForEntity($entity)
    {
        return static::find()->where([
            'entity' => $entity::className(),
            'target_id' => $entity->id,
        ])->count();
    }

    /**
     * Returns true if user have like on this entity
     * 
     * @param \app\models\User $user
     * @param \yii\db\ActiveRecord $entity
     * 
     * @return bool
     */
    public static function isUserLikedEntity($user, $entity) 
    {
        $likeModel = static::findOne([
            'entity' => $entity::className(),
            'target_id' => $entity->id,
            'user_id' => $user->id,
        ]);

        return ($likeModel !== null);
    }
}
