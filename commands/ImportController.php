<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\httpclient\Client;
use yii\console\Exception;
use yii\helpers\Html;
use app\models\Movie;

/**
 * Import 10 movies from iTunes
 */
class ImportController extends Controller
{
    /**
     * @var Client
     */
    private $httpClient;

    private const RSS_URL = 'https://trailers.apple.com/trailers/home/rss/newtrailers.rss';
    private const IMG_REGEXP = '/<img src="([^"]+)"/';
    
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->httpClient = new Client([
            'baseUrl' => self::RSS_URL,
        ]);
    }

    /**
     * Index action
     *
     * @throws Exception
     * @return integer
     */
    public function actionIndex(): int
    {
        $this->stdout("Starting movie import...\n");
        $this->stdout(sprintf("Import from %s\n", self::RSS_URL));

        $response = $this->httpClient->createRequest()->send();
        if(!$response->isOk) {
            throw new Exception(sprintf('Invalid response code: %s, expected: 200', $response->statusCode));
        }

        $this->stdout("Truncating movies table...\n");

        Yii::$app->db->createCommand()->truncateTable(Movie::tableName())->execute();
    
        $moviesImported = $this->processData($response->content);

        $this->stdout(sprintf("%d movies added succesfully.\n", $moviesImported));

        return ExitCode::OK;
    }

    /**
     * Process movie xml string
     *
     * @param string $data
     * @throws Exception
     * @return int
     */
    private function processData(string $data): int
    {
        $xml = (new \SimpleXMLElement($data))->children();
        $namespace = $xml->getNamespaces(true)['content'];

        if(!property_exists($xml, 'channel')) {
            throw new Exception("'channel' property not found in recieved array");
        }

        $idx = 0;

        foreach($xml->channel->item as $item) {
            if($idx === 10) {
                break;
            }

            $movie = $this->getMovie((string) $item->title);
            $movie->title = Html::encode((string) $item->title);
            $movie->link = Html::encode((string) $item->link);
            $movie->description = Html::encode((string) $item->description);
            $movie->pubDate = $this->parseDate((string) $item->pubDate);
            $movie->image = Html::encode($this->parseImageFromContent((string) $item->children($namespace)->encoded));

            $movie->save();

            $idx++;
        }

        return $idx;
    }

    /**
     * Parses trailer image from html content string
     *
     * @param string $content
     * @return string
     */
    private function parseImageFromContent(string $content): string
    {
        preg_match(self::IMG_REGEXP, $content, $matches);
        if(isset($matches[1])) {
            return $matches[1];
        }

        return "";
    }

    /**
     * Parses date string into timestamp
     *
     * @param string $date
     * @return string
     */
    private function parseDate(string $date): int
    {
        $dateTime = new \DateTime($date);
        
        return $dateTime->getTimestamp();
    }

    /**
     * Find movie by title, create if not exists
     *
     * @param string $title
     * @return Movie
     */
    private function getMovie(string $title): Movie
    {
        $movie = Movie::findOne(['title' => $title]);
        if($movie === null) {
            $movie = new Movie();
        }

        return $movie;
    }
}
