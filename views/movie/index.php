<?php
use yii\helpers\Html;
use app\models\Like;
use app\widgets\LikeWidget;
/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Movies list');
?>

<h1><?= $this->title ?></h1>
<div class="row">
    <?php foreach($movies as $movie): ?>
        <div class="col-xs-12 col-sm-6">
            <div class="thumbnail movie-card">
                <img src="<?= $movie->image ?>">
                <div class="caption">
                    <h3>
                        <?= Html::a($movie->title, ['movie/view', 'id' => $movie->id]) ?>
                    </h3>
                    <div class="movie-description" ><?= $movie->description ?></div>
                    <div class="movie-actions"> 
                        <?= Html::a(Yii::t('app', 'View'), ['movie/view', 'id' => $movie->id], [
                            'class' => 'btn btn-primary'
                        ]) ?>
                        <div style="float: right;">
                            <?= LikeWidget::widget([
                                'entity' => $movie,
                                'entityName' => 'movie',
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
