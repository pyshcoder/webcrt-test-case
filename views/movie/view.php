<?php
use yii\helpers\Html;
use app\widgets\LikeWidget;
/* @var $this yii\web\View */

$this->title = $movie->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="media">
        <div class="media-left media-middle">
            <img class="media-object" src="<?= $movie->image ?>">
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?= $movie->title ?></h4>
            <p>Trailer link: <a href="<?= $movie->link ?>" target="_blank"><?= $movie->link ?></a></p>
            <p>Published at: <span class="text-muted"><?= Yii::$app->formatter->asDatetime($movie->pubDate) ?></span></p>
            <p><?= $movie->description ?></p>
        </div>
    </div>
    <div class="movie-view-actions">
        <?= LikeWidget::widget([
            'entity' => $movie,
            'entityName' => 'movie',
        ]) ?>
    </div>
</div>
