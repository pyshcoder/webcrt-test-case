<?php

namespace app\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use app\models\Movie;

class MovieController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $movies = Movie::find()->all();

        return $this->render('index', [
            'movies' => $movies,
        ]);
    }

    public function actionView(int $id)
    {
        $movie = Movie::findOne(['id' => $id]);

        if($movie === null) {
            throw new NotFoundHttpException(Yii::t('app', 'Movie not found'));
        }

        return $this->render('view', [
            'movie' => $movie,
        ]);
    }
}
