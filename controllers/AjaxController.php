<?php

namespace app\controllers;

use Yii;
use yii\web\HttpException;
use app\models\Like;

class AjaxController extends \yii\web\Controller
{
    private const LIKE_ENTITIES = [
        'movie' => '\app\models\Movie',
    ];

    /**
     * @inheritDoc
     */
    public function beforeAction($action) 
    {   
        if(!\Yii::$app->request->isAjax) {
            throw new HttpException(400, 'Only ajax requests supported!');
        }
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        
        return parent::beforeAction($action);
    }

    /**
     * Toggle like ajax action
     *
     * @throws \Exception
     * @return array
     */
    public function actionToggleLike()
    {
        if(Yii::$app->user->isGuest) {
            throw new HttpException(403, 'Unauthorized');
        }

        $entity = Yii::$app->request->post('entity');
        $target_id = Yii::$app->request->post('target_id');
        
        if(!array_key_exists($entity, self::LIKE_ENTITIES) || !class_exists(self::LIKE_ENTITIES[$entity])) {
            return [
                'status' => 'error',
                'message' => Yii::t('app', 'Invalid like entity!'),
            ];
        }

        $entityModel = call_user_func([self::LIKE_ENTITIES[$entity], 'findOne'], ['id' => $target_id]);

        if($entityModel === null) {
            return [
                'status' => 'error',
                'message' => Yii::t('app', 'Entity not found!'),
            ];
        }

        $likeQuery = [
            'entity' => $entityModel::className(),
            'target_id' => $target_id,
            'user_id' => Yii::$app->user->identity->id,
        ];

        $likeModel = Like::findOne($likeQuery);

        if($likeModel === null) {
            $likeModel = new Like($likeQuery);
            $likeModel->save();
        } else {
            $likeModel->delete();
        }

        return [
            'status' => 'success',
        ];
    }
}
