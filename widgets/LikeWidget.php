<?php
namespace app\widgets;

use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\bootstrap\Button;
use app\assets\LikeAsset;
use app\models\Like;

class LikeWidget extends \yii\base\Widget
{
    /**
     * @var ActiveRecord
     */
    public $entity;

    /**
     * @var string
     */
    public $entityName;

    /**
     * @inheritdoc
     * @throws InvalidParamException
     */
    public function init()
    {
        parent::init();

        if (!isset($this->entity)) {
            throw new InvalidParamException(Yii::t('app', 'Entity must be set!'));
        }
        if (!isset($this->entityName)) {
            throw new InvalidParamException(Yii::t('app', 'Entity name must be set!'));
        }
        if(!($this->entity instanceof ActiveRecord)) {
            throw new InvalidParamException(Yii::t('app', 'Entity must be an instance of \yii\db\ActiveRecord'));
        }

        $this->view->registerAssetBundle(LikeAsset::class);
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $buttonLabel = sprintf('<span class="glyphicon glyphicon-heart like-icon"></span> <div class="like-divider">|</div> <div class="like-count">%d</div>', Like::countForEntity($this->entity));
        $buttonClass = 'button-like btn-default';

        if(!Yii::$app->user->isGuest) {
            $isLiked = Like::isUserLikedEntity(Yii::$app->user->identity, $this->entity);
            if($isLiked) {
                $buttonClass .= ' like-active';
            }
        }

        echo Button::widget([
            'label' => $buttonLabel,
            'encodeLabel' => false,
            'options' => [
                'disabled' => Yii::$app->user->isGuest,
                'class' => $buttonClass,

                'data-entity' => $this->entityName,
                'data-entity-id' => $this->entity->id,
            ]
        ]);
    }
}
